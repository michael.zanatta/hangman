def get_guessed_word(secret_word, guessed_characters):
    masked_word = ''
    for letter in secret_word:
        if letter in guessed_characters:
            masked_word += letter
        else:
            masked_word += '*'
    return masked_word


def check_guessed_word(secret_word, guessed_characters):
    masked_word = get_guessed_word(secret_word, guessed_characters)

    if '*' in masked_word:
        return False
    else:
        return True

secret_word='centralesupelec'
letters_guessed='baeltu'
print(get_guessed_word(secret_word , letters_guessed))
print(check_guessed_word(secret_word , letters_guessed))

